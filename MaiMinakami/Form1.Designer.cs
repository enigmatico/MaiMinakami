﻿namespace MaiMinakami
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.termoutput = new System.Windows.Forms.RichTextBox();
            this.terminput = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.termoutput, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.terminput, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 95.1049F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.895105F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(706, 572);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // termoutput
            // 
            this.termoutput.BackColor = System.Drawing.Color.Black;
            this.termoutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.termoutput.Location = new System.Drawing.Point(3, 3);
            this.termoutput.Name = "termoutput";
            this.termoutput.ReadOnly = true;
            this.termoutput.Size = new System.Drawing.Size(700, 538);
            this.termoutput.TabIndex = 0;
            this.termoutput.Text = "";
            this.termoutput.TextChanged += new System.EventHandler(this.termoutput_TextChanged);
            // 
            // terminput
            // 
            this.terminput.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.terminput.Location = new System.Drawing.Point(3, 549);
            this.terminput.Name = "terminput";
            this.terminput.Size = new System.Drawing.Size(700, 20);
            this.terminput.TabIndex = 1;
            this.terminput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.terminput_KeyDown);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 572);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = "Bot Console";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.RichTextBox termoutput;
        private System.Windows.Forms.TextBox terminput;
    }
}

