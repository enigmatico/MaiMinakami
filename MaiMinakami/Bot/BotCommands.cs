﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Xml;
using Discord;
using Discord.Commands;
using Newtonsoft.Json;
using MaiMinakami;

namespace MaiMinakami.Bot
{
    public class Command
    {
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        private string _alias;
        public string Alias
        {
            get
            {
                return _alias;
            }
            set
            {
                _alias = value;
            }
        }
        private string _help;
        public string Help
        {
            get
            {
                return _help;
            }
            set
            {
                _help = value;
            }
        }
        private int _args;
        public int Args
        {
            get
            {
                return _args;
            }
            set
            {
                _args = value;
            }
        }
    }

    [Remarks(" :tools: "), Summary("Utilities")]
    public class BotCommands : ModuleBase<ICommandContext>
    {
        [Command("wiki", RunMode = RunMode.Async), Summary("Searches the wikipedia")]
        public async Task WikiSearch([Remainder, Summary("Search parameters")] string search)
        {
            try
            {
                search = search.Replace(" ", "%20").ToLower();
                string html = string.Empty;
                string url = $@"https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&indexpageids&exintro=&explaintext=&titles={search}";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                }

                dynamic stuff = JsonConvert.DeserializeObject(html);
                string pageid = stuff.query.pageids[0];
                string artitle = stuff.query.pages[pageid].title;
                string artcontent = stuff.query.pages[pageid].extract;

                if (artcontent == null)
                {
                    await ReplyAsync($"Sorry, I can't find any relevant information about that topic. Try another search or try using other words.");
                    return;
                }

                if (artcontent.Length > 1995)
                {
                    artcontent = artcontent.Remove(1988, artcontent.Length - 1988);
                    artcontent += "(...)";
                }

                if (artcontent.Length > 0)
                {
                    await ReplyAsync($"```{artcontent}```");
                    EmbedBuilder eb = new EmbedBuilder();
                    //eb.WithTitle(artitle);
                    eb.WithDescription($@"About {artitle} on the Wikipedia (See https://en.wikipedia.org/wiki/{search})");
                    //eb.WithUrl($@"https://en.wikipedia.org/wiki/{search}");
                    //eb.WithDescription($"{artcontent}");
                    eb.WithColor(Color.DarkPurple);
                    await ReplyAsync("", false, eb);
                }
                else
                {
                    await ReplyAsync($"Sorry, I can't find any relevant information about that topic. Try another search or try using other words.");
                }
            }
            catch (Exception Exc)
            {
                BotUI.LError(Exc.ToString());
            }
        }

        [Command("gelbooru", RunMode = RunMode.Async), Summary("NSFW searches gelbooru for images.")]
        public async Task GelbooruSearch([Remainder, Summary("Search parameters (tags)")] string search)
        {
            try
            {
                search = search.Replace(" ", "+").ToLower();
                string html = string.Empty;
                string url = $@"https://gelbooru.com/index.php?page=dapi&s=post&q=index&tags={search}";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                }
                XmlDocument results = new XmlDocument();
                results.LoadXml(html);

                XmlNode root = results.DocumentElement;
                if(root.Attributes["count"] == null)
                {
                    await ReplyAsync($"Not found");
                    return;
                }
                int posts = Convert.ToInt32(root.Attributes["count"]?.InnerText);
                if (posts > 100)
                    posts = 100;
                if (posts < 1)
                {
                    await ReplyAsync($"Nothing to show to you.");
                    return;
                }
                int randval = new Random().Next(0, posts - 1);
                XmlNode selected = (XmlNode)root.ChildNodes[randval];
                string tags = selected.Attributes["tags"]?.InnerText;
                string source = selected.Attributes["file_url"]?.InnerText.Replace("//","https://");
                await ReplyAsync($"`{tags}`\n{source}");
            }
            catch(Exception Exc)
            {
                BotUI.LError(Exc.ToString());
            }
        }

        [Command("safebooru", RunMode = RunMode.Async), Summary("- SFW - searches safebooru for images.")]
        public async Task SafebooruSearch([Remainder, Summary("Search parameters (tags)")] string search)
        {
            try
            {
                search = search.Replace(" ", "+").ToLower();
                string html = string.Empty;
                string url = $@"https://safebooru.org/index.php?page=dapi&s=post&q=index&tags={search}";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                }
                XmlDocument results = new XmlDocument();
                results.LoadXml(html);

                XmlNode root = results.DocumentElement;
                if (root.Attributes["count"] == null)
                {
                    await ReplyAsync($"Not found");
                    return;
                }
                int posts = Convert.ToInt32(root.Attributes["count"]?.InnerText);
                if (posts > 100)
                    posts = 100;
                if (posts < 1)
                {
                    await ReplyAsync($"Nothing to show to you.");
                    return;
                }
                int randval = new Random().Next(0, posts - 1);
                XmlNode selected = (XmlNode)root.ChildNodes[randval];
                string tags = selected.Attributes["tags"]?.InnerText;
                string source = selected.Attributes["file_url"]?.InnerText.Replace("//", "https://");
                await ReplyAsync($"`{tags}`\n{source}");
            }
            catch (Exception Exc)
            {
                BotUI.LError(Exc.ToString());
            }
        }
    }
}
