﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Discord.Net;
using Discord;
using Discord.Commands;
using Discord.Audio;
using Discord.WebSocket;
using MaiMinakami;
using MaiMinakami.Chatbot.MaiSyn;
using MaiMinakami.Bot.Jukebox;

namespace MaiMinakami.Bot
{
    [Remarks(":desktop: "), Summary("Administration")]
    public class AdminModule : ModuleBase
    {
        [Command("siml"), Summary("Switches the chatbot to SIML mode"), RequireUserPermission(GuildPermission.Administrator)]
        public async Task Switchsiml()
        {
            try
            {
                if (MaiMinakami.Chatbot.MaiEther.MaiEther.Enabled)
                    MaiMinakami.Chatbot.MaiEther.MaiEther.Stop();
                MaySin.Initialize();
            }
            catch(Exception exc)
            {
                await ReplyAsync("[I HAS ERROR. TELL 3NIGMATICO TO FIX ME.]");
            }
            await ReplyAsync("I am now in SIML mode.");
        }

        [Command("ether"), Summary("Switches the chatbot to Etherbot mode"), RequireUserPermission(GuildPermission.Administrator)]
        public async Task Switchether()
        {
            try
            {
                if (MaySin.Enabled == true)
                    MaySin.Turnoff();
                MaiMinakami.Chatbot.MaiEther.MaiEther.Initialize();
            }
            catch (Exception exc)
            {
                await ReplyAsync("[I HAS ERROR. TELL 3NIGMATICO TO FIX ME.]");
            }
            await ReplyAsync("I am now in Ether mode.");
        }

        [Command("superp"), Summary("Enables quantum superposition chatbot mode. WARNING: Might explode!"), RequireUserPermission(GuildPermission.Administrator)]
        public async Task Superposition()
        {
            try
            {
                if (!MaySin.Enabled)
                    MaySin.Initialize();
                if(!MaiMinakami.Chatbot.MaiEther.MaiEther.Enabled)
                    MaiMinakami.Chatbot.MaiEther.MaiEther.Initialize();
            }
            catch (Exception exc)
            {
                await ReplyAsync("[I HAS ERROR. TELL 3NIGMATICO TO FIX ME.]");
            }
            await ReplyAsync("I am now in quantum superposition mode.");
        }
    }

[Remarks(":question: "), Summary("Information")]
    public class InfoModule : ModuleBase
    {
        public async Task<CommandInfo> CommandExists(string cmd)
        {
            List<CommandInfo> cil = BotToDiscord.GetCommands();
            foreach (CommandInfo ci in cil)
                if (ci.Name == cmd)
                    return ci;

            await Task.Delay(1);

            return null;
        }

        [Command("help"), Summary("Display a list of available commands")]
        public async Task Help([Remainder, Summary("Command")] string cmd = null)
        {
            if(cmd == null)
            {
                /*List<CommandInfo> cil = BotToDiscord.GetCommands();
                string clist = "";
                foreach (CommandInfo ci in cil)
                    clist += $"**`{ci.Name}`** ";

                EmbedBuilder eb = new EmbedBuilder();
                eb.WithDescription(clist);
                eb.WithColor(Color.DarkPurple);

                await ReplyAsync("**Available commands**\n\n", false, eb);*/
                List<ModuleInfo> mil = BotToDiscord.GetModules();
                foreach (ModuleInfo mi in mil)
                {
                    string modinfo = "";
                    modinfo += $"{mi.Remarks}*** {mi.Summary} ***\n\n";
                    foreach (CommandInfo ci in mi.Commands)
                        modinfo += $"**`{ci.Name}`** {ci.Summary}\n";
                    modinfo += "\n";
                    EmbedBuilder eb = new EmbedBuilder();
                    eb.WithDescription(modinfo);
                    eb.WithColor(Color.DarkPurple);
                    await ReplyAsync("", false, eb);
                }
            }
            else
            {
                CommandInfo mcmd = null;
                if ((mcmd = await CommandExists(cmd)) != null)
                {
                    string cmdh = $"Aliases: ";
                    foreach (string alias in mcmd.Aliases)
                        cmdh += $"**`{alias}`**";

                    cmdh += $"\n\n{mcmd.Summary}\n\n***Parameters:***\n";
                    foreach (Discord.Commands.ParameterInfo pi in mcmd.Parameters)
                        cmdh += $"**`{pi.Name}`** {pi.Summary}\n";

                    EmbedBuilder eb = new EmbedBuilder();
                    eb.WithDescription(cmdh);
                    eb.WithColor(Color.Orange);

                    await ReplyAsync($"**{cmd}**", false, eb);
                }
                else
                {
                    List<CommandInfo> cil = BotToDiscord.GetCommands();
                    string clist = "";
                    foreach (CommandInfo ci in cil)
                        clist += $"**`{ci.Name}`** ";

                    EmbedBuilder eb = new EmbedBuilder();
                    eb.WithDescription(clist);
                    eb.WithColor(Color.DarkPurple);

                    await ReplyAsync($"Command {cmd} does not exist. Available commands:\n\n", false, eb);
                }
            }
        }

        [Command("say"), Summary("Echos a message.")]
        public async Task Say([Remainder, Summary("The text to echo")] string echo)
        {
            // ReplyAsync is a method on ModuleBase
            await ReplyAsync(echo);
        }
    }

    /*[Remarks(":musical_note: "), Summary("Music playback")]
    public class Musicbox : ModuleBase
    {
        [Command("joinvoice", RunMode = RunMode.Async), Summary("Joins a voice channel")]
        public async Task JoinChannel([Remainder, Summary("Voice Channel")] IVoiceChannel channel = null)
        {
            try
            {
                // Get the audio channel
                channel = channel ?? (Context.Message.Author as IGuildUser)?.VoiceChannel;
                if (channel == null) { await ReplyAsync("User must be in a voice channel, or a voice channel must be passed as an argument."); return; }

                // For the next step with transmitting audio, you would want to pass this Audio Client in to a service.
                Jukebox.Jukebox.SetClient(await channel.ConnectAsync());
                //await channel.ConnectAsync();
                await ReplyAsync($"Joined " + channel.Name);
            }
            catch(Exception exc)
            {
                BotUI.LError(exc.ToString());
            }
            
        }

        [Command("debugplay"), Summary("Play a song")]
        public async Task JoinChannel([Remainder, Summary("Path to the song to play")] string path)
        {
            await Jukebox.Jukebox.SendAsync(path);
        }
    }*/

    public static class BotToDiscord
    {
        public static DiscordSocketClient BotClient;
        private static bool Running = false;
        private static Task ConnectTask;
        private static CommandService commands;
        private static IServiceProvider services;
        public static AudioService audios = new AudioService();
        public static M3U mPlaylist;

        public static List<ModuleInfo> GetModules()
        {
            List<ModuleInfo> mods = new List<ModuleInfo>();
            foreach (ModuleInfo mi in commands.Modules)
                mods.Add(mi);
            return mods;
        }

        public static List<CommandInfo> GetCommands()
        {
            List<CommandInfo> cmds = new List<CommandInfo>();
            foreach (CommandInfo ci in commands.Commands)
                cmds.Add(ci);
            return cmds;
        }

        private static async Task BotReady()
        {
            BotUI.Log("Ready");
            await Task.Delay(1);
            return;
        }

        public static async Task BotConnected()
        {
            BotUI.Log(BotClient.CurrentUser.Username + " connected");
            return;
        }

        private static async Task MessageUpdated(Cacheable<IMessage, ulong> before, SocketMessage after, ISocketMessageChannel channel)
        {
            var message = await before.GetOrDownloadAsync();
            //BotUI.Log($"{message} -> {after}");
            return;
        }

        public static async Task MessageReceived(SocketMessage message)
        {
            //if(!message.Author.IsBot)
            BotUI.Log("Message from: " + message.Author.Username);
            if (message.Author.Username != BotClient.CurrentUser.Username)
            {
                var sm = message as SocketUserMessage;
                //await message.Channel.SendMessageAsync(MaySin.Chat(message.Content, message.Author.Username));
                int argp = 0;
                if (!sm.HasStringPrefix(BotConfig.PREFIX, ref argp))
                {
                    if(MaiMinakami.Chatbot.MaiEther.MaiEther.Enabled && !MaiMinakami.Chatbot.MaiSyn.MaySin.Enabled)
                        await message.Channel.SendMessageAsync(MaiMinakami.Chatbot.MaiEther.MaiEther.Chat(message.Content));
                    else if(!MaiMinakami.Chatbot.MaiEther.MaiEther.Enabled && MaiMinakami.Chatbot.MaiSyn.MaySin.Enabled)
                        await message.Channel.SendMessageAsync(MaySin.Chat(message.Content, message.Author.Username));
                    else if(MaiMinakami.Chatbot.MaiEther.MaiEther.Enabled && MaiMinakami.Chatbot.MaiSyn.MaySin.Enabled)
                    {
                        
                        int rng = new Random().Next(0, 100);
                        if (rng >= 0 && rng <= 55)
                            await message.Channel.SendMessageAsync(MaiMinakami.Chatbot.MaiEther.MaiEther.Chat(message.Content));
                        else
                            await message.Channel.SendMessageAsync(MaySin.Chat(message.Content, message.Author.Username));
                    }

                }
                else
                {
                    /*var ctx = new SocketCommandContext(BotClient, sm);
                    var result = await commands.ExecuteAsync(ctx, BotConfig.PREFIX.Length-1, services);
                    if (!result.IsSuccess)
                        await ctx.Channel.SendMessageAsync(result.ErrorReason);*/
                    CommandContext context = new CommandContext(BotClient, sm);
                    IResult result = await commands.ExecuteAsync(context, argp, services);
                    if (!result.IsSuccess)
                        await context.Channel.SendMessageAsync(result.ErrorReason);
                }
            }
            else
            {
                //BotUI.LError("Message from self was rejected");
            }
            return;
        }

        public static async Task NetworkWatchDog()
        {
            while (Running)
            {
                await Task.Delay(10000);
                if (BotClient.ConnectionState == ConnectionState.Connecting || BotClient.ConnectionState == ConnectionState.Disconnected)
                {
                    BotUI.LError("Connection lost. Reconnecting...");
                    while(BotClient.ConnectionState != ConnectionState.Connected)
                    {
                        await BotClient.StopAsync();
                        //ConnectTask.Dispose();
                        Start();
                        await Task.Delay(10000);
                        if(BotClient.ConnectionState == ConnectionState.Connecting)
                        {
                            await Task.Delay(10000);
                            if (BotClient.ConnectionState == ConnectionState.Connected)
                                break;
                            else
                                BotUI.LError("Could not connect to discord. Retrying...");
                        }
                    }
                }
            }
        }

    public static void Start()
        {
            Running = true;
            commands = new CommandService();
            services = new ServiceCollection().BuildServiceProvider();

            ConnectTask = new Task(async()=> {

                await commands.AddModulesAsync(Assembly.GetEntryAssembly());
                //await commands.AddModuleAsync<InfoModule>();
                foreach(CommandInfo ci in commands.Commands)
                    BotUI.Log("Command: " + ci.Name + " registered");

                BotUI.Log("ConnectTask started");
                BotClient = new DiscordSocketClient();
                try
                {
                    BotClient.Ready += BotReady;
                    BotClient.Connected += BotConnected;
                    BotClient.MessageUpdated += MessageUpdated;
                    BotClient.MessageReceived += MessageReceived;
                    await BotClient.LoginAsync(TokenType.Bot, BotConfig.OAUTH);
                    await BotClient.StartAsync();
                    BotUI.Log("Bot started");
                    await NetworkWatchDog();
                }
                catch (Exception exc)
                {
                    BotUI.LError(exc.ToString());
                    await BotClient.StopAsync();
                    return;
                }
            });
            ConnectTask.Start();
        }

        public static void Stop()
        {
            Running = false;
            ConnectTask.Dispose();
            BotClient.StopAsync();
        }
    }
}
