﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using MaiMinakami;

namespace MaiMinakami.Bot
{
    public static class BotDB
    {
        private static SQLiteConnection sql_con;
        private static SQLiteCommand sql_cmd;

        public static void Initialize()
        {
            if (!System.IO.File.Exists("botinfo.db"))
                SQLiteConnection.CreateFile("botinfo.db");

            sql_con = new SQLiteConnection("Data Source=botinfo.db;Version=3;New=False;Compress=True;");
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();


            string[] create_table = new string[] { "CREATE TABLE servers(server TEXT UNIQUE)",
                "CREATE TABLE users(userid INT NOT NULL, userlevel INT NOT NULL DEFAULT 1, userexp UNSIGNED BIG INT NOT NULL DEFAULT 0," +
                "userrep INT NOT NULL DEFAULT 0, pats INT NOT NULL DEFAULT 0, credits UNSIGNED BIG INT NOT NULL DEFAULT 0)",
                "CREATE TABLE loans(userid INT NOT NULL, total UNSIGNED BIG INT NOT NULL, due UNSIGNED BIG INT NOT NULL," +
                    "intrate FLOAT NOT NULL, periodicy SMALLINT NOT NULL DEFAULT 0, signed DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP," +
                    "FOREIGN KEY (userid) REFERENCES users(userid))",
                "CREATE TABLE botver(botver INT NOT NULL DEFAULT 1)"};

            sql_cmd.CommandText = "SELECT name FROM sqlite_master WHERE type='table' AND name='table_name'";
            var tn = sql_cmd.ExecuteScalar();

            if(tn == null)
                foreach (string query in create_table)
                {
                    Bot.BotUI.Log("Creating database");
                    try
                    {
                        sql_cmd.CommandText = query;
                        sql_cmd.ExecuteNonQuery();
                        Bot.BotUI.Log("Done");
                    }
                    catch (Exception exc)
                    {
                        Bot.BotUI.LError("Could not execute " + query + ". Maybe it already exists.");
                    }
                }
        }

        public static void Close()
        {
            sql_con.Close();
        }
    }
}
