﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Discord;
using Discord.Audio;
using Discord.Commands;
using MaiMinakami;
using MaiMinakami.Bot.Jukebox;
using System.IO;

namespace MaiMinakami.Bot.Jukebox
{
    public static class JBSwitches
    {
        public static bool nextF = false;
        public static bool prevF = false;
        public static bool stopF = false;
        public static bool pstopF = false;
        public static bool pauseF = false;
        public static bool jumpF = false;
        public static int cur = 0;
    }

    [Remarks(":musical_note: "), Summary("Music playback")]
    public class AudioModule : ModuleBase<ICommandContext>
    {
        // Scroll down further for the AudioService.
        // Like, way down.
        // Hit 'End' on your keyboard if you still can't find it.
        private readonly AudioService _service;

        /*public AudioModule(AudioService service)
        {
            _service = service;
        }*/

        public AudioModule()
        {
            _service = BotToDiscord.audios;
            BotUI.Log("Created Jukebox");
        }

        // You *MUST* mark these commands with 'RunMode.Async'
        // otherwise the bot will not respond until the Task times out.
        [Command("joinvoice", RunMode = RunMode.Async), Summary("Makes the bot join a voice channel")]
        public async Task JoinCmd([Remainder, Summary("Voice Channel")] IVoiceChannel channel = null)
        {
            BotUI.Log("Joining VChannel");
            try
            {
                channel = channel ?? (Context.Message.Author as IGuildUser)?.VoiceChannel;
                if (channel == null) { await ReplyAsync("User must be in a voice channel, or a voice channel must be passed as an argument."); return; }
                await _service.JoinAudio(Context.Guild, channel);
            }
            catch(Exception Exc)
            {
                BotUI.LError(Exc.ToString());
            }
            BotUI.Log($"Joined {channel.Name}");
        }

        // Remember to add preconditions to your commands,
        // this is merely the minimal amount necessary.
        // Adding more commands of your own is also encouraged.
        [Command("leavevoice", RunMode = RunMode.Async), Summary("Makes the bot leave the voice channel")]
        public async Task LeaveCmd()
        {
            await _service.LeaveAudio(Context.Guild);
        }

        [Command("play", RunMode = RunMode.Async)]
        public async Task PlayCmd([Summary("Index of the song to play")] int song)
        {
            if(song < 0)
            {
                await ReplyAsync($"Bad index: {song.ToString()}");
            }else if(song >= BotToDiscord.mPlaylist.GetShortedList().Length)
            {
                await ReplyAsync($"There are only {BotToDiscord.mPlaylist.GetShortedList().Length.ToString()} songs in the playlist.");
            }

            try
            {
                JBSwitches.cur = song;
                JBSwitches.stopF = JBSwitches.jumpF = true;
                await Task.Delay(1);
                //BotUI.Log($"Playing {@song}");
                //await _service.SendAudioAsync(Context.Guild, Context.Channel, @song);
            }
            catch(Exception exc)
            {
                BotUI.LError(exc.ToString());
            }
            
        }

        [Command("pause", RunMode = RunMode.Async), Summary("Pauses the current playback")]
        public async Task PauseCmd()
        {
            try
            {
                JBSwitches.pauseF = !JBSwitches.pauseF;
                await ReplyAsync((JBSwitches.pauseF)?"The current playback has been paused":"Resuming playback");
            }
            catch (Exception exc)
            {
                BotUI.LError(exc.ToString());
            }

        }

        [Command("stop", RunMode = RunMode.Async), Summary("Stops the current playback")]
        public async Task StopCmd()
        {
            try
            {
                JBSwitches.stopF = JBSwitches.pstopF = true;
                await ReplyAsync("The current playback has been stopped.");
            }
            catch (Exception exc)
            {
                BotUI.LError(exc.ToString());
            }

        }

        [Command("next", RunMode = RunMode.Async), Summary("Skips to the next entry in the playlist, if any")]
        public async Task NextCmd()
        {
            try
            {
                JBSwitches.stopF = true;
                await Task.Delay(1);
            }
            catch (Exception exc)
            {
                BotUI.LError(exc.ToString());
            }

        }

        [Command("previous", RunMode = RunMode.Async), Summary("Skips to the previous entry in the playlist, if any")]
        public async Task PrevCmd()
        {
            try
            {
                JBSwitches.stopF = true;
                JBSwitches.prevF = true;
                await Task.Delay(1);
            }
            catch (Exception exc)
            {
                BotUI.LError(exc.ToString());
            }

        }

        [Command("start", RunMode = RunMode.Async), Summary("Starts playing a playlist")]
        public async Task StartCmd([Summary("Starting position in the playlist")] int at = -1)
        {
            if (at < 0)
                at = JBSwitches.cur;
            else if (at >= BotToDiscord.mPlaylist.GetShortedList().Length)
            {
                await ReplyAsync($"There are only {BotToDiscord.mPlaylist.GetShortedList().Length.ToString()} songs in the playlist. I can not start.");
                return;
            }

            try
            {
                PlaylistItem[] pl = BotToDiscord.mPlaylist.GetShortedList();
                for(int x = at; x < pl.Length; x++)
                {
                    if(JBSwitches.prevF)
                    {
                        x -= 2;
                        if (x < 0)
                            x = BotToDiscord.mPlaylist.GetShortedList().Length - 1;
                        JBSwitches.prevF = false;
                    }
                    if(JBSwitches.jumpF)
                    {
                       x = JBSwitches.cur;
                    }
                    JBSwitches.cur = x;
                    PlaylistItem pi = pl[x];
                    BotUI.Log($"Playing {pi.Name} ({Math.Floor((double)pi.Duration / 60).ToString()}:{(pi.Duration % 60).ToString()})");
                    await ReplyAsync($":musical_note: Now Playing {pi.Name} ({Math.Floor((double)pi.Duration / 60).ToString()}:{(pi.Duration % 60).ToString()})");
                    await _service.SendAudioAsync(Context.Guild, Context.Channel, pi.Path);
                    if (JBSwitches.pstopF)
                    {
                        JBSwitches.pstopF = false;
                        return;
                    }
                }
                //BotUI.Log($"Playing {@song}");
                //await _service.SendAudioAsync(Context.Guild, Context.Channel, @song);
            }
            catch (Exception exc)
            {
                BotUI.LError(exc.ToString());
            }

        }

        [Command("m3u", RunMode = RunMode.Async), Summary("Loads a playlist")]
        public async Task LoadM3U([Remainder] string m3u)
        {
            try
            {
                if(!File.Exists("PLAYLISTS\\" + m3u + ".m3u8"))
                {
                    await ReplyAsync("That playlist does not exist.");
                    return;
                }
                BotToDiscord.mPlaylist = new M3U(@m3u + ".m3u8");
                await ReplyAsync("Playlist " + m3u + " loaded!");
            }
            catch (Exception exc)
            {
                BotUI.LError(exc.ToString());
            }

        }

        [Command("lsplaylist", RunMode = RunMode.Async), Summary("List all songs in the current playlist")]
        public async Task ListPlaylist()
        {
            try
            {
                PlaylistItem[] pl = BotToDiscord.mPlaylist.GetShortedList();
                if(pl == null)
                {
                    await ReplyAsync("No playlist loaded. Load a playlist first using the m3u command.");
                    return;
                }
                string list = "";
                for(int x = 0; x < pl.Length; x++)
                {
                    list += $"`{x.ToString()} - {pl[x].Name} ({Math.Floor((double)pl[x].Duration / 60).ToString()}:{(pl[x].Duration % 60).ToString()})`\n";
                    if(list.Length >= 1850 || x == pl.Length - 1)
                    {
                        await ReplyAsync(list);
                        list = "";
                    }
                        
                }
            }
            catch (Exception exc)
            {
                BotUI.LError(exc.ToString());
            }

        }

        [Command("lsm3u", RunMode = RunMode.Async), Summary("List all playlists")]
        public async Task ListAll()
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo("PLAYLISTS\\");
                if(di.GetFiles().Length < 1)
                {
                    await ReplyAsync("There are no playlists available!");
                    return;
                }
                else
                {
                    string list = "";
                    FileInfo[] fi = di.GetFiles();
                    for (int x = 0; x < fi.Length; x++)
                    {
                        list += $"`{fi[x].Name.Replace(fi[x].Extension, "")} ({fi[x].CreationTime.ToString()})`\n";
                        if (list.Length >= 1850 || x == fi.Length - 1)
                        {
                            await ReplyAsync(list);
                            list = "";
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                BotUI.LError(exc.ToString());
            }
        }
    }
}
