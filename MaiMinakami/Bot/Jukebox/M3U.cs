﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace MaiMinakami.Bot.Jukebox
{
    public class PlaylistItem
    {
        private int _stime;
        public int Duration
        {
            get
            {
                return _stime;
            }
            set
            {
                _stime = value;
            }
        }
        private string _sname;
        public string Name
        {
            get
            {
                return _sname;
            }
            set
            {
                _sname = value;
            }
        }
        private string _spath;
        public string Path
        {
            get
            {
                return _spath;
            }
            set
            {
                _spath = value;
            }
        }

        public PlaylistItem(int duration, string name, string path)
        {
            _stime = duration;
            _sname = name;
            _spath = path;
        }
    }
    public class M3U
    {
        private List<PlaylistItem> tracklist;
        private PlaylistItem[] shortedlist;

        private readonly string regEntry = @"(?!#EXTINF:)[0-9]+.+";
        private readonly string pfolder = "PLAYLISTS\\";

        public M3U(string path)
        {
            if (!File.Exists(pfolder+path))
                return;

            //tracklist = new List<PlaylistItem>();
            shortedlist = new PlaylistItem[0];

            using (StreamReader fs = new StreamReader(pfolder + path))
            {
                string line = "";
                string name_ = "";
                string path_ = "";
                int duration_ = 0;
                bool goodfile = false;
                while ((line = fs.ReadLine()) != null)
                {
                    if(line.StartsWith("#EXTM3U"))
                    {
                        BotUI.Log("#EXTM3U header found. Good.");
                        goodfile = true;
                        continue;
                    }else if (line.StartsWith("#EXTINF"))
                    {
                        if (!goodfile)
                        {
                            BotUI.LError($"File {path} is not a valid playlist");
                            return;
                        }
                        string strip = Regex.Match(line, regEntry).Value;
                        string[] pams = strip.Split(new char[] { ',' });
                        name_ = pams[1];
                        duration_ = Convert.ToInt32(pams[0]);
                        BotUI.Log($"#EXTINF {name_} {duration_}");
                    }
                    else
                    {
                        if(!goodfile)
                        {
                            BotUI.LError($"File {path} is not a valid playlist");
                            return;
                        }
                        path_ = line;
                        PlaylistItem pit = new PlaylistItem(duration_, name_, path_);
                        BotUI.Log($"Adding: {pit.Duration.ToString()} {pit.Name} {pit.Path}");
                        //tracklist.Add(pit);
                        Array.Resize(ref shortedlist, shortedlist.Length + 1);
                        shortedlist[shortedlist.Length - 1] = pit;
                    }
                }
            }
        }

        public PlaylistItem[] GetShortedList()
        {
            return shortedlist;
        }

        public List<PlaylistItem> GetList()
        {
            return tracklist;
        }
    }
}
