﻿using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System;
using Discord;
using Discord.Audio;

namespace MaiMinakami.Bot.Jukebox
{
    public class AudioService
    {
        private readonly ConcurrentDictionary<ulong, IAudioClient> ConnectedChannels = new ConcurrentDictionary<ulong, IAudioClient>();

        public async Task JoinAudio(IGuild guild, IVoiceChannel target)
        {
            IAudioClient client;
            if (ConnectedChannels.TryGetValue(guild.Id, out client))
            {
                return;
            }
            if (target.Guild.Id != guild.Id)
            {
                return;
            }

            var audioClient = await target.ConnectAsync();

            if (ConnectedChannels.TryAdd(guild.Id, audioClient))
            {
                //await Log(LogSeverity.Info, $"Connected to voice on {guild.Name}.");
            }
        }

        public async Task LeaveAudio(IGuild guild)
        {
            IAudioClient client;
            if (ConnectedChannels.TryRemove(guild.Id, out client))
            {
                await client.StopAsync();
                //await Log(LogSeverity.Info, $"Disconnected from voice on {guild.Name}.");
            }
        }

        public async Task SendAudioAsync(IGuild guild, IMessageChannel channel, string path)
        {
            // Your task: Get a full path to the file if the value of 'path' is only a filename.
            if (!File.Exists(@path))
            {
                await channel.SendMessageAsync("File does not exist: " + @path);
                return;
            }
            IAudioClient client;
            if (ConnectedChannels.TryGetValue(guild.Id, out client))
            {
                BotUI.Log("Creating Stream");
                //await Log(LogSeverity.Debug, $"Starting playback of {path} in {guild.Name}");
                Process ffmpeg = CreateStream(@path);
                Stream output = ffmpeg.StandardOutput.BaseStream;
                //BotUI.Log("Length " + output.Length.ToString());
                BotUI.Log("Creating PCM output stream");
                // You can change the bitrate of the outgoing stream with an additional argument to CreatePCMStream().
                // If not specified, the default bitrate is 96*1024.
                var stream = client.CreatePCMStream(AudioApplication.Music);
                BotUI.Log("Streaming");

                int bsiz = 4089, rd = 0, tot = 0;
                byte[] buffer = new byte[bsiz];
                buffer.Initialize();
                await Task.Delay(3000);
                while((rd = await output.ReadAsync(buffer, 0, bsiz)) > 0)
                {
                    try
                    {
                        if(Bot.Jukebox.JBSwitches.stopF)
                        {
                            ffmpeg.Kill();
                            await stream.FlushAsync().ConfigureAwait(false);
                            output.Close();
                            stream.Close();
                            Bot.Jukebox.JBSwitches.stopF = false;
                            return;
                        }
                        if (Bot.Jukebox.JBSwitches.pauseF)
                        {
                            while (Bot.Jukebox.JBSwitches.pauseF)
                            {
                                if (Bot.Jukebox.JBSwitches.stopF)
                                {
                                    ffmpeg.Kill();
                                    await stream.FlushAsync().ConfigureAwait(false);
                                    output.Close();
                                    stream.Close();
                                    Bot.Jukebox.JBSwitches.pauseF = false;
                                    Bot.Jukebox.JBSwitches.stopF = false;
                                    return;
                                }
                                await Task.Delay(10);
                            }
                                
                        }
                        await stream.WriteAsync(buffer, 0, rd);
                        tot += rd;
                    }catch(Exception exc)
                    {
                        BotUI.LError(exc.ToString());
                        await stream.FlushAsync().ConfigureAwait(false);
                        output.Close();
                        stream.Close();
                        return;
                    }
                    
                }

                //await output.CopyToAsync(stream);
                await stream.FlushAsync().ConfigureAwait(false);
            }
        }

        private Process CreateStream(string @path)
        {
            return Process.Start(new ProcessStartInfo
            {
                FileName = "ffmpeg.exe",
                Arguments = $"-hide_banner -loglevel panic -i \"{@path}\" -ac 2 -f s16le -ar 48000 pipe:1",
                UseShellExecute = false,
                RedirectStandardOutput = true,
                WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
                CreateNoWindow = true
        });
        }
    }
}
