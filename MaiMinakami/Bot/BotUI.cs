﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Discord;

namespace MaiMinakami.Bot
{
    public static class BotUI
    {
        private static Form1 mForm;
        private static FileStream flog = new FileStream(@"LOGS/LOG" + DateTime.Today.ToShortDateString().Replace("/", "-") + ".LOG", FileMode.Append, FileAccess.Write);
        private static FileStream ferr = new FileStream(@"LOGS/ERROR" + DateTime.Today.ToShortDateString().Replace("/", "-") + ".LOG", FileMode.Append, FileAccess.Write);
        private static StreamWriter outlog = new StreamWriter(flog);
        private static StreamWriter outerr = new StreamWriter(ferr);
        public static Form1 FORM
        {
            set { mForm = value; }
        }

        public static void Log(string msg)
        {
            mForm.cmdoutput(msg, true);
            outlog.WriteLineAsync(msg);
        }

        public static void LError(string msg)
        {
            mForm.cmderror(msg);
            outerr.WriteLineAsync(msg);
        }

        public static void End()
        {
            outlog.Flush();
            outerr.Flush();
            outlog.Close();
            outerr.Close();
        }
    }
}
