﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaiMinakami.Bot;
using MaiMinakami.Properties;
using System.IO;
using Syn.Bot.Siml;
using Syn.EmotionML;

namespace MaiMinakami.Chatbot.MaiSyn
{
    public static class MaySin
    {
        public static SimlBot mBot = null;
        public static bool Enabled = false;
        public static void Initialize()
        {
            try
            {
                mBot = new SimlBot();
                mBot.Configuration.BotTimeout = new TimeSpan(TimeSpan.TicksPerSecond * 30);
                mBot.Learning += (sender, args) =>
                {
                    DirectoryInfo di = new DirectoryInfo(@"AI/" + BotPersona.AIMLFOLDER + "/LEARN");
                    string fname = Path.Combine(@"AI/" + BotPersona.AIMLFOLDER + "/LEARN", "Learning" + di.GetFiles().Length.ToString() + ".siml");
                    args.Document.Save(fname);
                    var fileString = File.ReadAllText(fname);
                    mBot.AddSiml(fileString);
                    fileString = null;
                    BotUI.Log("New Learn file: " + fileString);
                };

                if (!string.IsNullOrEmpty(BotPersona.BRAIN))
                {
                    string aidat = File.ReadAllText(@"AI/" + BotPersona.BRAIN);
                    mBot.PackageManager.LoadFromString(aidat);
                    aidat = null;
                }
                foreach (string packFile in Directory.GetFiles(@"AI/" + BotPersona.AIMLFOLDER, "*.simlpk"))
                {
                    string aidat = File.ReadAllText(packFile);
                    mBot.PackageManager.LoadFromString(aidat);
                    aidat = null;
                    BotUI.Log("Loaded " + packFile);
                }
                foreach (string simlFile in Directory.GetFiles(@"AI/" + BotPersona.AIMLFOLDER, "*.siml"))
                {
                    //System.Windows.Forms.MessageBox.Show("Loading " + simlFile);
                    var fileString = File.ReadAllText(simlFile);
                    mBot.AddSiml(fileString);
                    fileString = null;
                    BotUI.Log("Loaded " + simlFile);
                }
                foreach (string packFile in Directory.GetFiles(@"AI/" + BotPersona.AIMLFOLDER + "/LEARN", "*.simlpk"))
                {
                    string aidat = File.ReadAllText(packFile);
                    mBot.PackageManager.LoadFromString(aidat);
                    aidat = null;
                    BotUI.Log("Loaded " + packFile);
                }
                foreach (string simlFile in Directory.GetFiles(@"AI/" + BotPersona.AIMLFOLDER + "/LEARN", "*.siml"))
                {
                    //System.Windows.Forms.MessageBox.Show("Loading " + simlFile);
                    var fileString = File.ReadAllText(simlFile);
                    mBot.AddSiml(fileString);
                    fileString = null;
                    BotUI.Log("Loaded " + simlFile);
                }
                Enabled = true;
            }
            catch (Exception exc)
            {
                //System.Windows.Forms.MessageBox.Show("Error parsing the siml files: " + exc.ToString());
                BotUI.LError("Error parsing the siml files: " + exc.ToString());
                return;
            }
            BotUI.Log("Bot AI is ready!");
        }

        public static BotUser SearchBU(string name)
        {
            foreach (BotUser bu in mBot.Users)
                if (bu.ID == name)
                    return bu;

            return null;
        }

        public static string Chat(string input, string user)
        {
            ChatRequest cr = new ChatRequest(input.Replace("@" + BotConfig.USERNAME, "").Replace('@', ' '), mBot.MainUser);
            ChatResult chatResult = mBot.Chat(cr);
            return chatResult.BotMessage;
        }

        public static void Turnoff()
        {
            mBot.Release();
            mBot = null;
            Enabled = false;
            return;
        }
    }
}
