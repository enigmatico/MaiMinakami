﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using MaiMinakami;

namespace MaiMinakami.Chatbot.MaiEther
{
    public class Tuple
    {
        private string _word;
        public string Word
        {
            get
            {
                return _word;
            }
            set
            {
                _word = value;
            }
        }
        private int _repeats;
        public int Repeats
        {
            get
            {
                return _repeats;
            }
            set
            {
                _repeats = value;
            }
        }
        public Tuple(string word, int repeats)
        {
            _word = word;
            _repeats = repeats;
        }

        public int sumlength()
        {
            return _repeats * _word.Length;
        }

        public static int sum(List<Tuple> ws)
        {
            int res = 0;
            foreach (Tuple tp in ws)
                res += tp.sumlength();
            return res;
        }
    }

    public static class MaiEther
    {
        public static bool Enabled = false;
        static SQLiteConnection sql_con;
        static SQLiteCommand sql_cmd;
        static string[] bannedwords = {"I", "you", "he", "she", "we", "they", "it", "are", "is", "was", "will", "be", "do" };

        static long get_id(string entityName, string text)
        {
            string tableName = entityName + 's';
            string columnName = entityName;
            sql_cmd.CommandText = "SELECT rowid FROM " + tableName + " WHERE " + columnName + $" = '{text.Replace("'", "''")}'";
            try
            {
                using (SQLiteDataReader rdr = sql_cmd.ExecuteReader())
                {
                    if (rdr.HasRows)
                        while (rdr.Read())
                        {
                            //Bot.BotUI.Log("Rowid for " + text + ": " + rdr.GetInt64(0).ToString());
                            return rdr.GetInt64(0);
                        }
                    else
                    {
                        SQLiteCommand sql_cmd2 = sql_con.CreateCommand();
                        sql_cmd2.CommandText = "INSERT INTO " + tableName + " (" + columnName + $") VALUES ('{text.Replace("'", "''")}')";
                        sql_cmd2.ExecuteNonQuery();
                        sql_cmd2.CommandText = "SELECT last_insert_rowid()";
                        return (long)sql_cmd2.ExecuteScalar();
                    }
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
            }

            return 0;
        }

        static List<Tuple> Counter(string[] words)
        {
            List<Tuple> wordList = new List<Tuple>();
            var word_query = (from string word in words
                              orderby word
                              select word).Distinct();
            string[] result = word_query.ToArray();

            foreach (string n in result)
            {
                int reps = 0;
                for (int x = 0; x < words.Length; x++)
                {
                    if (words[x] == n)
                        reps++;
                }
                Tuple a = new Tuple(n, reps);
                wordList.Add(a);
            }
            return wordList;
        }

        static List<Tuple> get_words(string text)
        {
            string[] words = new string[0];
            string wordsRegexpString = "(?:\\w+[?\\w+'\".;\\-,!@#$%^&*()\\\\/|<>]+)";
            foreach (Match m in Regex.Matches(text.ToLower(), wordsRegexpString))
            {
                Array.Resize(ref words, words.Length + 1);
                words[words.Length - 1] = m.Value;
            }
            return Counter(words);
        }

        public static async void Initialize()
        {
            try
            {
                if (!System.IO.File.Exists("bot.db"))
                    SQLiteConnection.CreateFile("bot.db");

                sql_con = new SQLiteConnection("Data Source=bot.db;Version=3;New=False;Compress=True;");
                await sql_con.OpenAsync();
                sql_cmd = sql_con.CreateCommand();
            }
            catch (Exception exc)
            {
                Bot.BotUI.LError(exc.ToString());
            }

            string[] create_table = new string[] { "CREATE TABLE words(word TEXT UNIQUE)",
                "CREATE TABLE sentences(sentence TEXT UNIQUE, used INT NOT NULL DEFAULT 0)",
                "CREATE TABLE associations (word_id INT NOT NULL, sentence_id INT NOT NULL, weight REAL NOT NULL)" };
            foreach (string query in create_table)
            {
                try
                {
                    sql_cmd.CommandText = query;
                    sql_cmd.ExecuteNonQuery();
                }
                catch (Exception exc)
                {
                    Bot.BotUI.LError("Could not execute " + query + ". Maybe it already exists.");
                }
            }
            Enabled = true;
        }

        public static void Stop()
        {
            
            if(sql_con != null)
            {
                sql_con.Close();
                sql_con.Dispose();
                sql_con = null;
            }
            if(sql_cmd != null)
            {
                sql_cmd.Dispose();
                sql_cmd = null;
            }
            Enabled = false;
            return;
        }

        static string B = "Hello!";
        public static string Chat(string Input)
        {
            //string B = "";
            string H = Input;
            if (String.IsNullOrEmpty(H))
                return "[I AM ERROR]";

            H = Regex.Replace(H, @"((((http)|(https)|(magnet)|(ftp)|(file)):\/\/)(www.)?[a-zA-Z0-9_-]+(.[a-zA-Z0-9_-]+)?[a-zA-Z0-9]{1,4}(/[a-zA-Z0-9?$?:=,;._-]+)?)|www.[a-zA-Z0-9_-]+.[a-zA-Z0-9]{1,4}(/[a-zA-Z0-9?$?:=,;._-]+)?", "[URL]");
            H = Regex.Replace(H, @"[a-zA-Z0-9]+?(.|_|-)?[a-zA-Z0-9]+@[a-zA-Z0-9]+?(.|_|-)?[a-zA-Z0-9]+.[a-zA-Z0-9]{1,4}", "[MAIL]");

            List<Tuple> words = get_words(B);
            int words_length = Tuple.sum(words);
            long sentence_id = get_id("sentence", H);
            foreach (Tuple tp in words)
            {
                long word_id = get_id("word", tp.Word);
                long weight = (long)Math.Sqrt(tp.Repeats / words_length);
                sql_cmd.CommandText = $"INSERT INTO associations VALUES ({word_id.ToString()}, {sentence_id.ToString()}, {weight.ToString()})";
                sql_cmd.ExecuteNonQuery();
                //sql_cmd.CommandText = $"INSERT INTO words VALUES ('{tp.Word}',{sentence_id.ToString()})";
            }

            //Retrieve answer
            sql_cmd.CommandText = "CREATE TEMPORARY TABLE results(sentence_id INT, sentence TEXT, weight REAL)";
            sql_cmd.ExecuteNonQuery();
            words = get_words(H);
            words_length = Tuple.sum(words);
            foreach (Tuple tp in words)
            {
                if(!bannedwords.Contains(tp.Word))
                {
                    long weight = (long)Math.Sqrt(tp.Repeats / words_length);
                    sql_cmd.CommandText = $@"INSERT INTO results SELECT associations.sentence_id, sentences.sentence, {weight.ToString()}*associations.weight/(4+sentences.used) FROM words INNER JOIN associations ON associations.word_id=words.rowid INNER JOIN sentences ON sentences.rowid=associations.sentence_id WHERE words.word='{tp.Word.Replace("'", "''")}'";
                    sql_cmd.ExecuteNonQuery();
                }
            }

            sql_cmd.CommandText = "SELECT sentence_id, sentence, SUM(weight) AS sum_weight FROM results GROUP BY sentence_id ORDER BY sum_weight DESC LIMIT 4";
            SQLiteCommand sql_cmd2 = new SQLiteCommand(sql_con);
            sql_cmd2.CommandText = "SELECT count(*) FROM (SELECT sentence_id, sentence, SUM(weight) AS sum_weight FROM results GROUP BY sentence_id ORDER BY sum_weight DESC LIMIT 4)";

            bool HasRows = false;
            long rows = (long)sql_cmd2.ExecuteScalar();

            try
            {
                using (SQLiteDataReader rdr = sql_cmd.ExecuteReader())
                {
                    if (HasRows = rdr.HasRows)
                    {
                        rdr.Read();
                        if (rows > 1)
                        {
                            int rng = new Random().Next(0, 100);
                            if (rng <= 70)
                            {
                                B = rdr.GetString(1);
                                sentence_id = rdr.GetInt64(0);
                            }
                            else if (rng > 70 && rng <= 85)
                            {
                                rdr.Read();
                                B = rdr.GetString(1);
                                sentence_id = rdr.GetInt64(0);
                            }
                            else if (rng > 85 && rng <= 95 && rows > 2)
                            {
                                rdr.Read();
                                rdr.Read();
                                B = rdr.GetString(1);
                                sentence_id = rdr.GetInt64(0);
                            }
                            else if (rng > 95 && rng <= 100 && rows > 3)
                            {
                                rdr.Read();
                                rdr.Read();
                                rdr.Read();
                                B = rdr.GetString(1);
                                sentence_id = rdr.GetInt64(0);
                            }
                            else
                            {
                                B = rdr.GetString(1);
                                sentence_id = rdr.GetInt64(0);
                            }

                        }
                        else
                        {
                            B = rdr.GetString(1);
                            sentence_id = rdr.GetInt64(0);
                        }
                    }
                }
            }catch(Exception exc)
            {
                Bot.BotUI.LError("Query error: " + exc.ToString());
                sql_cmd.CommandText = "DROP TABLE results";
                sql_cmd.ExecuteNonQuery();
                return "[I AM ERROR. CALL 3NIGMATICO TO FIX ME.]";

            }

            sql_cmd.CommandText = "DROP TABLE results";
            sql_cmd.ExecuteNonQuery();

            if (!HasRows)
            {
                sql_cmd.CommandText = "SELECT rowid, sentence FROM sentences WHERE used = (SELECT MIN(used) FROM sentences) ORDER BY RANDOM() LIMIT 1";
                using (SQLiteDataReader rdr = sql_cmd.ExecuteReader())
                {
                    rdr.Read();
                    B = rdr.GetString(1);
                    sentence_id = rdr.GetInt64(0);
                }
            }

            sql_cmd.CommandText = $"UPDATE sentences SET used=used+1 WHERE rowid={sentence_id.ToString()}";
            sql_cmd.ExecuteNonQuery();
            return B;
        }
    }
}
