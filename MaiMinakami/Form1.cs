﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaiMinakami;

namespace MaiMinakami
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        public void cmdoutput(string text)
        {
            termoutput.SelectionStart = termoutput.TextLength;
            termoutput.SelectionLength = 0;
            Color original = termoutput.SelectionColor;
            termoutput.SelectionColor = Color.LightGreen;
            termoutput.AppendText("\n" + text);
            termoutput.SelectionColor = original;
        }

        public void cmderror(string text)
        {
            termoutput.SelectionStart = termoutput.TextLength;
            termoutput.SelectionLength = 0;
            Color original = termoutput.SelectionColor;
            termoutput.SelectionColor = Color.Red;
            termoutput.AppendText("\n" + text);
            termoutput.SelectionColor = original;
        }

        public void cmdoutput(string text, bool newline)
        {
            termoutput.SelectionStart = termoutput.TextLength;
            termoutput.SelectionLength = 0;
            Color original = termoutput.SelectionColor;
            termoutput.SelectionColor = Color.LightGreen;
            if (newline)
                termoutput.AppendText("\n" + text);
            else
                termoutput.AppendText(text);
            termoutput.SelectionColor = original;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Bot.BotUI.FORM = this;
            Bot.ConfigManager.InitConfigManager();
            Bot.BotDB.Initialize();
            //MaiMinakami.Chatbot.MaiSyn.MaySin.Initialize();
            MaiMinakami.Chatbot.MaiEther.MaiEther.Initialize();
            Bot.BotToDiscord.Start();
        }

        private void termoutput_TextChanged(object sender, EventArgs e)
        {
            termoutput.SelectionStart = termoutput.TextLength;
            termoutput.ScrollToCaret();
        }

        private void terminput_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                //Send input
                cmdoutput(terminput.Text, true);
                terminput.Text = "";
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Bot.BotToDiscord.Stop();
            MaiMinakami.Chatbot.MaiEther.MaiEther.Stop();
        }
    }
}
